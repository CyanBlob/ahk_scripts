MouseIsOver(WinTitle) {
    MouseGetPos,,, Win
    return WinExist(WinTitle " ahk_id " Win)
}

#if !MouseIsOver("ahk_class ahk_class WorkerW")
Alt & LButton::
CoordMode, Mouse ;Switch to screen/absolute coordinates.
MouseGetPos, EWD_MouseStartX, EWD_MouseStartY, EWD_MouseWin
WinGetPos, EWD_OriginalPosX, EWD_OriginalPosY,,, ahk_id %EWD_MouseWin%
WinGet, EWD_WinState, MinMax, ahk_id %EWD_MouseWin% 
FullScreen := true
if (EWD_WinState = 0)
{
    FullScreen := false
}
SetTimer, EWD_IncrementalMove, 1 ;Track the mouse as the user drags it.
return
#if

EWD_IncrementalMove:
GetKeyState, EWD_LButtonState, LButton, P
if EWD_LButtonState = U  ;LMB released
{
    SetTimer, EWD_IncrementalMove, off
    return
}
GetKeyState, EWD_EscapeState, Escape, P
if EWD_EscapeState = D  ;Escape cancels drag
{
    SetTimer, EWD_IncrementalMove, off
    WinMove, ahk_id %EWD_MouseWin%,, %EWD_OriginalPosX%, %EWD_OriginalPosY%
    return
}

CoordMode, Mouse
MouseGetPos, EWD_MouseX, EWD_MouseY
WinGetPos, EWD_WinX, EWD_WinY,,, ahk_id %EWD_MouseWin%

;If near the top of the screen, maximize the window
if (!FullScreen && EWD_MouseY <= 5)
{
    PrevWindow := WinExist("A")
    WinActivate, ahk_id %EWD_MouseWin%
    Send #{Up}
    WinActivate, ahk_id %PrevWindow%
    FullScreen := true
}
;If previously fullscreened and the mouse moves away, minimize the window
if (FullScreen && EWD_MouseY > 5)
{
    PrevWindow := WinExist("A")
    WinActivate, ahk_id %EWD_MouseWin%
    Send #{Down}
    WinActivate, ahk_id %PrevWindow%
    FullScreen := false
}
;If the window wasn't maximized, move the window
if (!FullScreen)
{
    ;Pull current mouse position again since the cursor could have moved while fullscreened
    CoordMode, Mouse
    MouseGetPos, EWD_MouseX, EWD_MouseY
    WinGetPos, EWD_WinX, EWD_WinY,,, ahk_id %EWD_MouseWin%
    SetWinDelay, -1   ; Makes the below move faster/smoother.
    WinMove, ahk_id %EWD_MouseWin%,, EWD_WinX + EWD_MouseX - EWD_MouseStartX, EWD_WinY + EWD_MouseY - EWD_MouseStartY
    EWD_MouseStartX := EWD_MouseX  ; Update for the next timer-call to this subroutine.
    EWD_MouseStartY := EWD_MouseY
}
return
